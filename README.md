# Dactl Ansible Playbook

Used to install `dactl` using `ansible`.

## Setup

Common setup to run a single ansible playbook, for anything more use Google. Assumes that `ansible` has been installed.

```sh
sudo mkdir -p /etc/ansible
echo -e "[local]\nlocalhost ansible_connection=local" | \
  sudo tee /etc/ansible/hosts
```

## Usage

```sh
git clone git://gitlab.com/crdc/playbooks/dactl.yml
ansible-playbook --ask-become-pass dactl/playbook.yml
```
